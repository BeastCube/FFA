package net.beastcube.ffa;

import lombok.Getter;
import net.beastcube.api.bukkit.BeastCubeAPI;
import net.beastcube.api.bukkit.config.annotation.AnnotationConfiguration;
import net.beastcube.api.bukkit.util.scoreboard.SimpleScoreboard;
import net.beastcube.api.commons.CuboidRegion;
import net.beastcube.api.commons.minigames.MinigameState;
import net.beastcube.api.commons.minigames.MinigameType;
import net.beastcube.api.commons.players.Coins;
import net.beastcube.minigameapi.Minigame;
import net.beastcube.minigameapi.MinigameAPI;
import net.beastcube.minigameapi.countdowns.Countdowns;
import net.beastcube.minigameapi.defaults.Defaults;
import net.beastcube.minigameapi.defaults.PlayerAmountBoundType;
import net.beastcube.minigameapi.defaults.PlayerAmountBounds;
import net.beastcube.minigameapi.events.PlayerIngameDeathEvent;
import net.beastcube.minigameapi.events.PlayerIngameJoinEvent;
import net.beastcube.minigameapi.scoreboard.ScoreboardMode;
import net.beastcube.minigameapi.team.Team;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author BeastCube
 */
@Minigame(arena = FFAArena.class, team = Team.class, minigameType = MinigameType.FFA, scoreboardMode = ScoreboardMode.NO_SCOREBOARD)
public class FFA extends JavaPlugin implements Listener {

    @Getter
    private static MinigameAPI<FFAArena, Team, AnnotationConfiguration> minigameAPI;
    @Getter
    private static FFA instance;
    public static HashMap<UUID, Integer> deaths = new HashMap<>();
    public static HashMap<UUID, Integer> kills = new HashMap<>();
    public static HashMap<UUID, Integer> killstreaks = new HashMap<>();
    private HashMap<Location, BukkitTask> removeBlockTasks = new HashMap<>();
    private String TNTBOWDISPLAYNAME = ChatColor.AQUA + "TNT Bow";
    private Map<UUID, Long> cooldowns = new HashMap<>();
    private Map<UUID, Long> banCooldowns = new HashMap<>();

    int cooldownTime = 15; //SECONDS
    int banTime = 60; //SECONDS

    @Override
    public void onEnable() {
        BeastCubeAPI.setFriendsTeleportEnabled(false);
        instance = this;

        Defaults defaults = new Defaults();
        defaults.setBlockBreaking(false);
        defaults.setBlockPlacing(false);
        defaults.setContainerOpening(false);
        defaults.setHunger(false);
        defaults.setCrafting(false);
        defaults.setAutoRespawn(true);
        defaults.setBlockExploding(false);
        defaults.setDropStuffOnDeath(false);
        defaults.setDisplayIngameCountdown(false);
        defaults.setFriendlyFire(true);
        defaults.setKeepInventoryOnDeath(false);
        defaults.setDaylightCycle(false);
        defaults.setEnterBedAllowed(false);
        defaults.setDestroySoil(false);
        defaults.setHangingBreak(false);
        defaults.setTeamChat(false);
        defaults.setBlockBurning(false);
        defaults.setTntDamage(true);
        defaults.setDropItems(false);
        defaults.setJoinIngame(true);
        defaults.setIngameCountdown(0);

        minigameAPI = new MinigameAPI<>(this, new PlayerAmountBounds(PlayerAmountBoundType.NONE), FFAArena.class, Team.class, defaults);
        Countdowns.clearCountdowns();

        Bukkit.getPluginManager().registerEvents(this, this);

        minigameAPI.loadRandomArena();

        minigameAPI.setState(MinigameState.INGAME);
    }

    @Override
    public void onDisable() {

    }

    @EventHandler
    public void onPlayerIngameDeath(PlayerIngameDeathEvent e) {
        final Player p = e.getPlayer();
        final Player killer = p == p.getKiller() ? null : p.getKiller();
        e.setDeathMessage("");

        if (!deaths.containsKey(p.getUniqueId()))
            deaths.put(p.getUniqueId(), 0);
        if (!kills.containsKey(p.getUniqueId()))
            kills.put(p.getUniqueId(), 0);
        if (!killstreaks.containsKey(p.getUniqueId()))
            killstreaks.put(p.getUniqueId(), 0);

        if (killer != null) {
            if (!deaths.containsKey(killer.getUniqueId()))
                deaths.put(killer.getUniqueId(), 0);
            if (!kills.containsKey(killer.getUniqueId()))
                kills.put(killer.getUniqueId(), 0);
            if (!killstreaks.containsKey(killer.getUniqueId()))
                killstreaks.put(killer.getUniqueId(), 0);
            //reward
            Coins.addCoins(killer.getUniqueId(), 1);
            p.sendMessage(ChatColor.GRAY + "Du wurdest von " + ChatColor.RED + killer.getName() + ChatColor.GRAY + " getötet! Er hatte noch " + (double) Math.round(10 * killer.getHealth() / 2) / 10 + " Herzen.");
            killer.sendMessage(ChatColor.GRAY + "Du hast " + ChatColor.RED + p.getName() + ChatColor.GRAY + " getötet!");
            killer.sendMessage(ChatColor.GREEN + "Du hast 1 Coin erhalten!");
            kills.put(killer.getUniqueId(), kills.get(killer.getUniqueId()) + 1);
            killstreaks.put(killer.getUniqueId(), killstreaks.get(killer.getUniqueId()) + 1);
            UpdateSB(killer);
            killer.setHealth(killer.getHealth() + 10D > 20 ? 20 : killer.getHealth() + 10D);
            killer.getInventory().addItem(new ItemStack(Material.ARROW, 5));
            //-------------Killstreak--------------
            int killstreak = killstreaks.get(killer.getUniqueId());
            if (killstreak == 3) {
                killer.getInventory().addItem(new ItemStack(Material.WEB, 3));
            } else if (killstreak == 5) {
                killer.getInventory().addItem(new ItemStack(Material.TNT, 5));
                //} else if (killstreak == 7) {
                //killer.getInventory().addItem(new ItemBuilder(Material.FLINT_AND_STEEL).durability(60).build());
            }
            //TODO Giant Killstreak
            if (killstreak == 3 || killstreak == 5 /*|| killstreak == 7*/) {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    player.sendMessage(ChatColor.RED + killer.getName() + ChatColor.YELLOW + " hat eine " + killstreak + "er Killstreak!");
                }
            }
            cooldowns.entrySet().stream().filter(entry -> entry.getKey() == killer.getUniqueId()).forEach(cooldowns::remove);
        } else {
            e.setDeathMessage(ChatColor.RED + p.getName() + ChatColor.GRAY + " ist gestorben!");
        }
        deaths.put(p.getUniqueId(), deaths.get(p.getUniqueId()) + 1);
        killstreaks.put(p.getUniqueId(), 0);

        addInventoryItems(e.getInventory().getInventory(), e.getInventory().getArmorContents());
        cooldowns.entrySet().stream().filter(entry -> entry.getKey() == p.getUniqueId()).forEach(cooldowns::remove);
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        if (cooldowns.containsKey(p.getUniqueId()) && cooldowns.get(p.getUniqueId()) <= System.currentTimeMillis()) {
            banCooldowns.put(p.getUniqueId(), System.currentTimeMillis() + banTime * 1000);
            Bukkit.getScheduler().scheduleSyncDelayedTask(this, () -> {
                if (banCooldowns.containsKey(p.getUniqueId())) {
                    banCooldowns.remove(p.getUniqueId());
                }
            }, banTime * 1000);
        }
        if (cooldowns.containsKey(p.getUniqueId())) {
            cooldowns.remove(p.getUniqueId());
        }
    }


    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent e) {
        UpdateSB(e.getPlayer());
    }

    @EventHandler
    public void onPlayerPlayerLogin(PlayerLoginEvent e) {
        final Player p = e.getPlayer();
        if (banCooldowns.containsKey(p.getUniqueId()) && banCooldowns.get(p.getUniqueId()) > System.currentTimeMillis()) {
            e.setResult(PlayerLoginEvent.Result.KICK_OTHER);
            e.setKickMessage(ChatColor.RED + String.format("Du bist noch für %s Sekunden gebannt, da du den Kampf frühzeitig verlassen hast!", (banCooldowns.get(p.getUniqueId()) - System.currentTimeMillis()) / 1000));
            return;
        }
        if (banCooldowns.containsKey(p.getUniqueId())) {
            banCooldowns.remove(p.getUniqueId());
        }
    }

    @EventHandler
    public void onPlayerIngameJoin(PlayerIngameJoinEvent e) {
        final Player p = e.getPlayer();
        if (killstreaks.containsKey(p.getUniqueId())) {
            killstreaks.remove(p.getUniqueId());
        }
        if (!kills.containsKey(p.getUniqueId()))
            kills.put(p.getUniqueId(), 0);
        if (!deaths.containsKey(p.getUniqueId()))
            deaths.put(p.getUniqueId(), 0);
        addInventoryItems(e.getInventory(), e.getInventoryArmorContents());
        UpdateSB(p);
    }

    private void addInventoryItems(Player p) {
        addInventoryItems(p.getInventory(), p.getInventory().getArmorContents());
    }

    private void addInventoryItems(Inventory inventory, ItemStack[] armor) {
        armor[3] = (new ItemStack(Material.IRON_HELMET));
        armor[2] = (new ItemStack(Material.IRON_CHESTPLATE));
        armor[1] = (new ItemStack(Material.IRON_LEGGINGS));
        armor[0] = (new ItemStack(Material.IRON_BOOTS));

        ItemStack sword = new ItemStack(Material.IRON_SWORD);
        sword.addUnsafeEnchantment(Enchantment.DURABILITY, 10);

        ItemStack fishingrod = new ItemStack(Material.FISHING_ROD);
        fishingrod.addUnsafeEnchantment(Enchantment.DURABILITY, 10);

        ItemStack bow = new ItemStack(Material.BOW);
        bow.addUnsafeEnchantment(Enchantment.DURABILITY, 10);

        inventory.setItem(0, sword);
        inventory.setItem(1, fishingrod);
        inventory.setItem(2, bow);

        inventory.setItem(8, new ItemStack(Material.ARROW, 20));

    }

    private void UpdateSB(Player p) {
        SimpleScoreboard sb = new SimpleScoreboard(ChatColor.YELLOW + "Free for all");
        double kill = kills.get(p.getUniqueId());
        double death = deaths.get(p.getUniqueId());
        Integer killstreak = killstreaks.get(p.getUniqueId());
        sb.blank();
        sb.setLine(ChatColor.GOLD + "Kills");
        sb.setLine(ChatColor.GREEN + String.valueOf(kill));
        sb.blank();
        sb.setLine(ChatColor.GOLD + "Deaths");
        sb.setLine(ChatColor.RED + String.valueOf(death));
        sb.blank();
        sb.setLine(ChatColor.GOLD + "K/D");
        sb.setLine(ChatColor.AQUA + "" + round(kill / (death == 0 ? 1 : death), 2));
        sb.blank();
        sb.setLine(ChatColor.GOLD + "Killstreak");
        sb.setLine(ChatColor.AQUA + "" + (killstreak == null ? 0 : killstreak));
        sb.build();
        sb.send(p);
    }

    public static double round(double valueToRound, int numberOfDecimalPlaces) {
        double multipicationFactor = Math.pow(10, numberOfDecimalPlaces);
        double interestedInZeroDPs = valueToRound * multipicationFactor;
        return Math.round(interestedInZeroDPs) / multipicationFactor;
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        Block block = e.getBlockPlaced();
        e.setCancelled(false);
        if (block.getType() == Material.WEB) {
            removeBlockAfter(block.getLocation(), 10 * 20);
        } else if (block.getType() == Material.TNT) {
            block.setType(Material.AIR);
            TNTPrimed tnt = block.getLocation().getWorld().spawn(block.getLocation(), TNTPrimed.class);
            tnt.setYield(3);
        } else /*if (block.getType() != Material.FIRE)*/ {
            e.setCancelled(!minigameAPI.getDefaults().isBlockPlacing());
        }
    }

    public void removeBlockAfter(final Location loc, int ticks) {
        BukkitTask task = Bukkit.getScheduler().runTaskLater(this, () -> loc.getBlock().setType(Material.AIR), ticks);
        removeBlockTasks.put(loc, task);
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        e.setCancelled(false);
        Location loc = e.getBlock().getLocation();
        if (e.getBlock().getType() == Material.WEB) {
            if (removeBlockTasks.containsKey(loc)) {
                removeBlockTasks.get(loc).cancel();
                removeBlockTasks.remove(loc);
            }
        } else /*if (e.getBlock().getType() != Material.FIRE)*/ {
            e.setCancelled(!minigameAPI.getDefaults().isBlockPlacing());
        }
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) { //TODO Arrows can hurt players
        if (e.getDamager() instanceof Player && e.getEntity() instanceof Player) {
            Player damager = (Player) e.getDamager();
            Player damaged = (Player) e.getEntity();
            CuboidRegion spawnRegion = minigameAPI.getArena().getSpawnRegion();
            if (spawnRegion != null) {
                if (spawnRegion.intersects(damager.getLocation().getBlock().getLocation())) { //TODO 0.3 bounding box
                    e.setCancelled(true);
                    damager.sendMessage(ChatColor.RED + "Du darfst im Spawnbereich keine Spieler angreifen!");
                    return;
                } else if (spawnRegion.intersects(damaged.getLocation().getBlock().getLocation())) {
                    e.setCancelled(true);
                    damager.sendMessage(ChatColor.RED + "Du darfst nicht einen Spieler im Spawnbereich angreifen!");
                    return;
                }
            }
            cooldowns.put(damager.getUniqueId(), System.currentTimeMillis() + cooldownTime * 1000);
            cooldowns.put(damaged.getUniqueId(), System.currentTimeMillis() + cooldownTime * 1000);
        } else if (e.getEntity() instanceof Player) {
            CuboidRegion spawnRegion = minigameAPI.getArena().getSpawnRegion();
            if (spawnRegion != null && spawnRegion.intersects(e.getEntity().getLocation())) {
                e.setCancelled(true);
            } else {
                e.setCancelled(false);
            }
        }
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent e) { //TODO TNT not making damage
        if (e.getEntity() instanceof Player) {
            Player p = (Player) e.getEntity();

            CuboidRegion spawnRegion = minigameAPI.getArena().getSpawnRegion();
            if (spawnRegion != null && spawnRegion.intersects(p.getLocation())) {
                e.setCancelled(true);
            }
        }
    }

}
