package net.beastcube.ffa;

import lombok.Getter;
import lombok.Setter;
import net.beastcube.api.bukkit.commands.Command;
import net.beastcube.api.bukkit.util.serialization.Serialize;
import net.beastcube.api.commons.CuboidRegion;
import net.beastcube.minigameapi.PlayerState;
import net.beastcube.minigameapi.arena.Arena;
import net.beastcube.minigameapi.arena.Status;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * @author BeastCube
 */
public class FFAArena extends Arena {
    @Serialize
    @Status(name = "Spawn", command = "/spawn set")
    @Getter
    @Setter
    private Location spawn;
    @Serialize
    @Status(name = "Spawnbereich")
    @Getter
    @Setter
    private CuboidRegion spawnRegion;

    private Location pos1;
    private Location pos2;

    public FFAArena() {
        super();
    }

    public FFAArena(Location spawn, Location spectatorSpawn, List<String> builders, ItemStack icon, int colorSet, String name) {
        super(spectatorSpawn, builders, icon, colorSet, name);
        this.spawn = spawn;
    }

    @Command(identifiers = "spawn set", description = "Setzt den Spawn an der aktuellen Position")
    public void spawnCommand(Player sender) {
        this.setSpawn(sender.getLocation());
        sender.sendMessage(ChatColor.GREEN + "Der Spawn wurde erfolgreich gesetzt.");
    }

    public Location getSpawn(Player p) {
        return FFA.getMinigameAPI().getPlayerState(p) != PlayerState.INGAME && FFA.getMinigameAPI().getPlayerState(p) != PlayerState.IN_LOBBY ? this.getSpectatorSpawn() : this.spawn;
    }

    @Command(identifiers = "spawn pos1", description = "Setzt den ersten Punkt des Spawnbereiches")
    public void spawnPos1Command(Player sender) {
        Location loc = sender.getLocation();
        Location pos = new Location(loc.getWorld(), loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
        Location pos2 = spawnRegion == null ? this.pos2 : spawnRegion.getV2Location();
        if (pos2 == null) {
            pos1 = pos;
            sender.sendMessage(ChatColor.GREEN + "Der erste Punkt des Spawnbereiches wurde erfolgreich gesetzt");
        } else if (pos.getWorld() == pos2.getWorld()) {
            pos1 = pos;
            spawnRegion = new CuboidRegion(pos1.toVector(), pos2.toVector(), pos1.getWorld());
            sender.sendMessage(ChatColor.GREEN + "Der erste Punkt des Spawnbereiches wurde erfolgreich gesetzt. Spawnbereich aktualisiert");
        } else {
            sender.sendMessage("Der erste und der zweite Punkt sind nicht in der gleichen Welt");
        }
    }

    @Command(identifiers = "spawn pos2", description = "Setzt den zweiten Punkt des Spawnbereiches")
    public void spawnPos2Command(Player sender) {
        Location loc = sender.getLocation();
        Location pos = new Location(loc.getWorld(), loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
        Location pos1 = spawnRegion == null ? this.pos1 : spawnRegion.getV1Location();
        if (pos1 == null) {
            pos2 = pos;
            sender.sendMessage(ChatColor.GREEN + "Der zweite Punkt des Spawnbereiches wurde erfolgreich gesetzt");
        } else if (pos.getWorld() == pos1.getWorld()) {
            pos2 = pos;
            spawnRegion = new CuboidRegion(pos1.toVector(), pos2.toVector(), pos1.getWorld());
            sender.sendMessage(ChatColor.GREEN + "Der zweite Punkt des Spawnbereiches wurde erfolgreich gesetzt. Spawnbereich aktualisiert");
        } else {
            sender.sendMessage("Der erste und der zweite Punkt sind nicht in der gleichen Welt");
        }
    }

}
